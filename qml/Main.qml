/*
 * Copyright (C) 2020  David Germiquet
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * BongoCatDialer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick 2.0
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'bongocatdialer.trulycanadian'

    automaticOrientation: true
    backgroundColor: UbuntuColors.blue
    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('BongoCat Dialer')
        }
            Grid {
                anchors {
                top: header.bottom
                topMargin: units.gu(5)
                }
                columns: 3
                spacing: 10
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13)
                MouseArea {
                        anchors.fill: parent
                        onClicked: {
                                     parent.color = 'red'
                                     python.call('test.speak', ['1'], function(returnValue) {
                                               })
                        }

                }
                }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
                Rectangle { color: "white"; width: units.gu(13); height: units.gu(13) }
            }
    }

   Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('bin/'));
            addImportPath(Qt.resolvedUrl('bin/appEnvironment/lib64/python3.5/site-packages'));
            addImportPath(Qt.resolvedUrl('bin/appEnvironment/lib/python3.5/site-packages'));
            importModule('test', function(){
                console.log('module imported');
            });
        }
        onError: {
            console.log('python error: ' + traceback);
        }
    }
}
