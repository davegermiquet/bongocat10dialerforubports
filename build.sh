#!/bin/bash

env
mkdir -p $INSTALL_DIR/bin
python3 -m venv $INSTALL_DIR/bin/appEnvironment
source $INSTALL_DIR/bin/appEnvironment/bin/activate
pip install pyserial
pip install python-gsmmodem-new


cp -r ${ROOT}/qml ${INSTALL_DIR}/bin/
cp -r ${ROOT}/assets $INSTALL_DIR/bin/
cp ${ROOT}/src/call_Dialer.sh $INSTALL_DIR/bin/call_Dialer.sh
cp ${ROOT}/src/test.py $INSTALL_DIR/bin/test.py
cp ${ROOT}/src/main.py $INSTALL_DIR/bin/main.py
cp ${ROOT}/src/__init__.py $INSTALL_DIR/bin/__init__.py

cd ${ROOT}/
cmake .
cp manifest.json ${INSTALL_DIR}
cp BongoCat10MeowDialer.desktop.in ${INSTALL_DIR}/BongoCat10MeowDialer.desktop
cp BongoCat10MeowDialer.apparmor ${INSTALL_DIR}/BongoCat10MeowDialer.apparmor
echo "Done"



